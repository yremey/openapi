# adblockplus nofeed







安装说明

chrome 和 firefox 的 Adblock Plus 插件

![image-20200407102848908](https://tva1.sinaimg.cn/large/00831rSTgy1gdkzutze9kj31jq0k60wf.jpg)

订阅地址 URL

https://gitee.com/gitbay/adblockplus-nofeed/raw/master/adblockplus-nofeeds.txt





## 高级技巧 通配符





class为  rightContent-3YRklhu9 

例如 https://news.ifeng.com/c/7vTcUgXCRzO

可以使用通配符

>  news.ifeng.com##[class^='rightContent-']

对应的JavaScript为

```JavaScript
$("[class^='rightContent-']")
```





## 工具箱子

在线字符串排序

https://www.bejson.com/othertools/stringarraysort/

在线去除重复字符串

https://www.bejson.com/othertools/removeDuplicate/

## 其他过滤

屏蔽网页js挖矿
https://github.com/hoshsadiq/adblock-nocoin-list

https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt


## 网络设置

使用技巧

![image-20200410084654927](https://tva1.sinaimg.cn/large/00831rSTgy1gdodrqki9xj31f00u00xy.jpg)




