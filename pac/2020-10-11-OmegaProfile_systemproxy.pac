var FindProxyForURL = function(init, profiles) {
    return function(url, host) {
        "use strict";
        var result = init, scheme = url.substr(0, url.indexOf(":"));
        do {
            result = profiles[result];
            if (typeof result === "function") result = result(url, host, scheme);
        } while (typeof result !== "string" || result.charCodeAt(0) === 43);
        return result;
    };
}("+systemproxy", {
    "+systemproxy": function(url, host, scheme) {
        "use strict";
        if (/^wikimedia\.org$/.test(host)) return "+proxy";
        if (/^127\.0\./.test(host)) return "DIRECT";
        if (/^0\.0\./.test(host)) return "DIRECT";
        if (/^192\.168\./.test(host)) return "DIRECT";
        if (/(?:^|\.)cn$/.test(host)) return "DIRECT";
        if (/(?:^|\.)qq\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)taobao\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)baidu\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)bdimg\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)bcebos\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)bdstatic\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)zhimg\.com$/.test(host)) return "DIRECT";
        if (/^amazonaws\.com$/.test(host)) return "DIRECT";
        if (/(?:^|\.)medium\.com$/.test(host)) return "+proxy";
        if (/^medium\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)twimg\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)fbcdn\.net$/.test(host)) return "+proxy";
        if (/(?:^|\.)dropbox\.com$/.test(host)) return "+proxy";
        if (/^dropboxstatic\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)dropboxstatic\.com$/.test(host)) return "+proxy";
        if (/^dropbox\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)dropboxusercontent\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)facebook\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)facebook\.net$/.test(host)) return "+proxy";
        if (/^goo\.gl$/.test(host)) return "+proxy";
        if (/(?:^|\.)goo\.gl$/.test(host)) return "+proxy";
        if (/(?:^|\.)google\.cn$/.test(host)) return "+proxy";
        if (/^google\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)google\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googleadservices\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googleapis\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googleblog\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googlesource\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googlesyndication\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googletagmanager\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googleusercontent\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)googlevideo\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)gstatic\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)githubusercontent\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)youtu\.be$/.test(host)) return "+proxy";
        if (/(?:^|\.)youtube-nocookie\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)youtube\.com$/.test(host)) return "+proxy";
        if (/^twitter\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)twitter\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)stackadapt\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)mathtag\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)github\.io$/.test(host)) return "+proxy";
        if (/(?:^|\.)w3schools\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)redd\.it$/.test(host)) return "+proxy";
        if (/(?:^|\.)reddit\.com$/.test(host)) return "+proxy";
        if (/(?:^|\.)redditstatic\.com$/.test(host)) return "+proxy";
        return "DIRECT";
    },
    "+proxy": function(url, host, scheme) {
        "use strict";
        if (/^127\.0\..* \+direct$/.test(host) || /^0\.0\..* \+direct$/.test(host) || /^192\.168\..* \+direct$/.test(host) || /\.cn \+direct$/.test(host) || /\.qq\.com \+direct$/.test(host) || /\.taobao\.com \+direct$/.test(host) || /\.baidu\.com \+direct$/.test(host) || /\.bdimg\.com \+direct$/.test(host) || /\.bcebos\.com \+direct$/.test(host) || /\.bdstatic\.com \+direct$/.test(host) || /\.gitbay\.com \+direct$/.test(host) || /\.gitbay\.net \+direct$/.test(host)) return "DIRECT";
        return "SOCKS5 127.0.0.1:7070; SOCKS 127.0.0.1:7070";
    }
});